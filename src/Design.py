# -*- coding: utf-8 -*-
# Copyright 2015, 2016 Shiquan Wang
# 
# This file is part of SimGrasp.
# 
# SimGrasp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SimGrasp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SimGrasp.  If not, see <http://www.gnu.org/licenses/>.

# Created on Tue Nov 10 22:56:50 2015

"""
.. codeauthor:: Shiquan Wang

The Design module creates :class:`Hand`, :class:`Finger`, and
:class:`GraspObject` objects for simulations. The module creates the objects
based on user-given dimensions and geometries, and outputs the generated
objects to Klamp't ``.rob`` files.
"""

from klampt import so3, se3    #for math operations
import math
import subprocess
import BlenderTransforms
#Global Variables
HAND_FILE_PATH = '/home/tyler/projects/stanford/sim_grasp/sim-grasp/data/hands/'
OBJECT_FILE_PATH = '/home/tyler/projects/stanford/sim_grasp/sim-grasp/data/objects/'

def run_blender_script(args):
    """
    Run a blender python script to transform geometries from ``.stl`` files.
    Arguments are ``FILE -s SCALE [-o OUTPUT_FILE]``.

    :param list[str] args: The arguments to pass to the blender script.
    """
    subprocess.call(["blender", "-b", "-P", BlenderTransforms.__file__, "--"] + args)

class Hand():
    """
    Generates a hand robot for simulating with Klamp't.
    """
    def __init__(self, palmPos = [0, 0, 0.1]):
        """
        :param list[float] palmPos: The position of the palm in 3D space.
        """
        # fingerIndex: from 0 to n-1
        # palmPos: (list) the position of the palm center (center of the palm surface plane)
        # controllableFingerNum: number of fingers whose base can be controlled
        self.fingerNum = 0        
        self.controllableFingerNum = 0
        self.palmPos = palmPos
        self.fingerPhalangesNumList = []
        self.fingerFileNameList = []
        self.fingerTypeList = []
        self.fingerOrientList = []
        self.fingerBasePosList = []
        self.baseJointMassList = []
        self.baseJointInitPosList = []
        
        self.baseJointTorqueLimit = []
        self.baseJointPosLimit = []
        self.baseJointVelLimit = []
        self.baseJointAccLimit = []  
        self.setControllerPID([50, 0 , 10])
        self.setDryFriction(1)
        self.setViscousFriction(3)     
        self.setPalmGeometry('cylinder_palm.stl', 0.1, 1)
        self.setfingerBaseGeometry('cylinder_base.stl', 0.03)
        self.handFileName = None
        
    def addFinger(self, finger, fingerOrient, fingerBasePos, fingerType = 'fixedBase'):
        """
        Adds a new finger to the hand with the given orientation and position.
        The order in which fingers does matter when setting their parameters
        later on.

        :param Finger finger: The :class:`Finger` to add to the hand.
        :param float fingerOrient: The orientation of the finger on the palm
            plane in degrees. 0 deg points in the direction of the positive x
            axis.
        :param list[float] fingerBasePos: The position of the finger base
            rotary/tilted center in the format ``[X, Y, Z]``.
        :param str fingerType: Determines how the finger base may move. Must be
            either ``'fixedBase'``, ``'rotaryBase'``, or ``'tiltedBase'``.
        """
        assert (finger.fingerFileName != None), "Please build your finger file first!"
        self.fingerNum += 1
        if fingerType != 'fixedBase':
            self.controllableFingerNum += 1 
        self.fingerPhalangesNumList.append(finger.phalangesNum)
        self.fingerFileNameList.append(finger.fingerFileName)
        self.fingerTypeList.append(fingerType)
        self.fingerOrientList.append(fingerOrient)
        self.fingerBasePosList.append(fingerBasePos)
        self.baseJointMassList.append(0.1)
        self.baseJointInitPosList.append(0)   
        
        self.baseJointTorqueLimit.append(10)        
        self.baseJointPosLimit.append((-30, 30))
        self.baseJointVelLimit.append(90)
        self.baseJointAccLimit.append(180)
        
    def setPalmPos(self, palmPos):
        """
        :param list[float] palmPos: The position of the palm in 3D space in the
            form ``[X, Y, Z]``.
        """
        assert (len(palmPos) == 3), "input dimensions should be 3!"
        self.palmPos = palmPos
        
    def setBaseJointMass(self, baseJointMassList):
        """
        :param list[float] baseJointMassList: The masses of the base of each
            finger in the hand in the order in which the fingers were added to
            the hand.
        """
        assert (len(baseJointMassList) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointMassList = baseJointMassList

    def setBaseJointInitPos(self, baseJointInitPosList):
        """
        :param list[int] baseJointInitPosList: The initial rotation of each base
            joint in degrees in the order in which the fingers were added to the
            hand.
        """
        assert (len(baseJointInitPosList) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointInitPosList = baseJointInitPosList

    def setBaseJointTorqueLimit(self, baseJointTorqueLimit):
        """
        :param list[int] baseJointTorqueLimit: The maximum torque that can be
            applied to each finger in the order in which the fingers were added
            to the hand.
        """
        assert (len(baseJointTorqueLimit) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointTorqueLimit = baseJointTorqueLimit      

    def setBaseJointPosLimit(self, baseJointPosLimit):
        """
        :param list[list[int]] baseJointPosLimit: A list of the min and max
            angles each base joint can rotate in degrees. The list for a hand
            with *N* fingers should contain *N* lists of two integers. The first
            should be the minimum angle, and the second should be the maximum
            angle. The order of the limits should be the same as the order in
            which the fingers were added to the hand.
        """
        assert (len(baseJointPosLimit) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointPosLimit = baseJointPosLimit

    def setBaseJointVelLimit(self, baseJointVelLimit):
        """
        :param list[int] baseJointVelLimit: A list of the maximum angular
            velocity of each base joint in deg/sec in the order in which the
            fingers were added to the hand.
        """
        assert (len(baseJointVelLimit) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointVelLimit = baseJointVelLimit

    def setBaseJointAccLimit(self, baseJointAccLimit):
        """
        :param list[int] baseJointAccLimit: A list of the maximum angular
            acceleration of each base joint in deg/sec\ :superscript:`2`.
            The order should be the same as the order in which the fingers were
            added to the hand.
        """
        assert (len(baseJointAccLimit) == self.fingerNum), "input dimensions don't match with number of fingers!"
        self.baseJointAccLimit = baseJointAccLimit
        
    def setControllerPID(self, PID):
        """
        :param list[float] PID: PID values separated into a list.
        """
        assert (len(PID) == 3), "Number of input variables are not 3!" 
        self.PID = PID
    
    def setDryFriction(self, dryFriction):
        """
        :param float dryFriction: Coefficient of dry friction.
        """
        self.dryFriction = dryFriction
    
    def setViscousFriction(self, viscousFriction):
        """
        :param float viscousFriction: Coefficient of viscous friction.
        """
        self.viscousFriction = viscousFriction
        
    def setPalmGeometry(self, palmGeometryFile, palmDiameter = 0.1, palmMass = 1):
        """
        Sets the geometry and physical attributes of the palm.
        In your geometry file, you should make sure that the:

        - Palm plane (palm surface) points towards the Z-axis.
        - X and Y coordinates are also consistent with the global
            coordinates (used to define the finger orientation and
            position).
        - Origin is located on the palm plane.

        :param str palmGeometryFile: Path to the file containing the palm
            geometry.
        :param float palmDiameter: How much to scale the palm geometry.
        :param int palmMass: The mass of the palm object in the simulation.
        """
        self.palmGeometryFile = palmGeometryFile
        self.palmDiameter = palmDiameter
        self.palmMass = palmMass
        
    def setfingerBaseGeometry(self, fingerBaseGeometryFile, fingerBaseDiameter = 0.03):
        """
        The origin is at the center of the geometry for default geometry.

        :param str fingerBaseGeometryFile: Path to the file containing the
            finger base geometry, relative to the ``data/hands`` directory.
        :param float fingerBaseDiameter: How much to scale the finger base
            geometry.
        """
        self.fingerBaseGeometryFile = fingerBaseGeometryFile
        self.fingerBaseDiameter = fingerBaseDiameter
    
    def getControllableDofInfo(self):
        """
        Get an ordered list of controllable base joints and joints. The list
        for a finger with *N* fingers and *M* joints per finger is in the format
        ``['BaseJoint_Finger0', 'BaseJointFinger1', ... , 'BaseJointFingerN-1',
        'Joint0_Finger0', 'Joint1_Finger0', ... , 'JointM-1_FingerN-1']``.

        :return: A list of controllable DOFs.
        :rtype: list[str]
        """
        infoList = []
        #adding base joint
        for i in range(self.fingerNum):
            if self.fingerTypeList[i] != 'fixedBase':
                infoList.append('BaseJoint_' + 'Finger' + str(i))
        for i in range(self.fingerNum):
            for j in range(self.fingerPhalangesNumList[i]):
                infoList.append('Joint' + str(j) + '_Finger' + str(i))
        return infoList
        
    def getControllableDofIndex(self, fingerIndex, jointIndex):
        """
        Dof index used in the controller (0 based index, doesn't include fixed joints)

        :param int fingerIndex: Which finger to get the DOF from.
        :param int jointIndex: Which joint of the finger (starting from -1 at
            the base).
        """
        assert (fingerIndex >= 0 and fingerIndex <= self.fingerNum-1 and jointIndex >= -1 and jointIndex <= self.fingerPhalangesNumList[fingerIndex] - 1), "Index out of range!"
        assert (jointIndex > -1 or (jointIndex == -1 and self.fingerTypeList[fingerIndex] != 'fixedBase')), "Finger " + str(fingerIndex) + " does not have a rotary base!"
        index = 0   

        if jointIndex == -1:   #base joint
            for i in range(fingerIndex):
                if self.fingerTypeList[i] != 'fixedBase':
                    index += 1
        else:
            #counting the base joint
            for i in range(self.fingerNum):
                if self.fingerTypeList[i] != 'fixedBase':
                    index += 1
            #counting the rest joints
            for i in range(fingerIndex):
                index += self.fingerPhalangesNumList[i]
            index += jointIndex
        return index

    def getSensorizedDofIndex(self, fingerIndex, jointIndex):
        """
        Dof index that is consistent with sensor data (0 based index).

        :param int fingerIndex: Which finger to get the DOF from.
        :param int jointIndex: Which joint of the finger (starting from -1 at
            the base).
        """
        assert (fingerIndex >= 0 and fingerIndex <= self.fingerNum-1 and jointIndex >= -1 and jointIndex <= self.fingerPhalangesNumList[fingerIndex] - 1), "Index out of range!"
        assert (jointIndex > -1 or (jointIndex == -1 and self.fingerTypeList[fingerIndex] != 'fixedBase')), "Finger " + str(fingerIndex) + " does not have a rotary base!"
        if jointIndex == -1:
            return fingerIndex + 1
        else:
            index = 1 + self.fingerNum #the first several dofs are palm and base joints
            #summing up for the fingers before Finger #fingerIndex        
            for i in range(fingerIndex):
                index += self.fingerPhalangesNumList[i]
            #summing up for the Finger #fingerIndex
            index += jointIndex
            return index
    
    def getLinkIndex(self, fingerIndex, phalanxIndex): 
        """
        link index used in .rob file (0 based index).

        :param int fingerIndex: Which finger to get the DOF from.
        :param int phalanxIndex: Which phalanx of the finger (starting from 0 at
            the base).
        """
        assert (fingerIndex >= 0 and phalanxIndex >= -1), "Index out of range!"
        if phalanxIndex == -1:
            return fingerIndex + 1
        else:
            return sum([self.fingerPhalangesNumList[i] for i in range(fingerIndex)]) + self.fingerNum + phalanxIndex + 1
        
    def buildFile(self, handFileName = 'hand_default'):
        """
        Build a Klamp't ``.rob`` file for the hand.

        :param str handFileName: Path to where the ``.rob`` file should be
            saved.
        """
        self.handFileName = handFileName
        handFile = open(HAND_FILE_PATH + handFileName + '.rob', 'w')
        handFile.write('### Hand File ###\n\n')
        #Hand configurations (parents, transformation, initial positions)
        handFile.write('TParent \\\n')
        handFile.write('  '.join([str(item) for item in (so3.identity() + self.palmPos)]))
        #Compute transformation matrix for each finger
        for i in range(self.fingerNum):
            handFile.write(' \\\n')
            #for global coordinates to transform to finger coordinates with 0 deg finger orientation
            #1) rotate x by 90deg and translate to base joint pos, 2) then rotate y by (finger orient) deg
            #T2 = (so3.rotation([1, 0, 0], math.radians(90)), self.fingerBasePosList[i])
            T1 = (so3.identity(), self.fingerBasePosList[i])
            T2 = (so3.rotation((1, 0, 0), math.radians(90)), [0, 0, 0])
            T3 = (so3.rotation((0, 1, 0), math.radians(self.fingerOrientList[i])), [0, 0, 0])
            T12 = se3.mul(T1, T2)
            T = se3.mul(T12, T3)
            handFile.write(' '.join([str(round(item, 5)) for item in (so3.inv(T[0]) + T[1])]))
        handFile.write('\n\n')
        handFile.write('  '.join(['parents'] + [str(-1)] + [str(0)] * self.fingerNum) + '\n')
        #axis define the dof axis with respect to the child coordinates
        jointBaseAxisList = []
        for fingerIndex in range(self.fingerNum):
            if self.fingerTypeList[fingerIndex] == 'tiltedBase':    
                jointBaseAxisList.append('1 0 0')
            else:   #case of fixed and rotary base
                jointBaseAxisList.append('0 1 0')
        handFile.write('  '.join(['axis'] + ['0 0 1'] + jointBaseAxisList) + '\n\n')   #z axis of the finger is along the finger joint axis    
        #Joint limit
        handFile.write('  '.join(['qMinDeg'] + ['0'] + [str(item[0]) for item in self.baseJointPosLimit]) + '\n')
        handFile.write('  '.join(['qMaxDeg'] + ['0'] + [str(item[1]) for item in self.baseJointPosLimit]) + '\n')
        handFile.write('  '.join(['velMaxDeg'] + ['0'] + [str(item) for item in self.baseJointVelLimit]) + '\n')
        handFile.write('  '.join(['accMaxDeg'] + ['0'] + [str(item) for item in self.baseJointAccLimit]) + '\n')
        handFile.write('  '.join(['torqueMax'] + ['inf'] + [str(item) for item in self.baseJointTorqueLimit]) + '\n\n')
        #Initial position and finger base mass
        handFile.write('  '.join(['qDeg'] + ['0'] + [str(item) for item in self.baseJointInitPosList]) + '\n')
        handFile.write('  '.join(['mass'] + [str(self.palmMass)] + [str(item) for item in self.baseJointMassList]) + '\n')
        handFile.write('automass\n\n')
        #Geometry
        handFile.write('  '.join(['geometry'] + ['\"' + self.palmGeometryFile + '\"'] + ['\"' + self.fingerBaseGeometryFile + '\"'] * self.fingerNum) + '\n')
        geometryScaleList = []
        if self.palmGeometryFile == 'cylinder_palm.stl':
            geometryScaleList.append(str(self.palmDiameter))
        else:
            geometryScaleList.append('1')
        if self.fingerBaseGeometryFile == 'cylinder_base.stl':
            for i in range(self.fingerNum):
                geometryScaleList.append(str(self.fingerBaseDiameter))
        else:
            for i in range(self.fingerNum):
                geometryScaleList.append('1')
        handFile.write('  '.join(['geomscale'] + geometryScaleList) + '\n\n')
        #Finger base joint type
        handFile.write('  '.join(['joint'] + ['weld'] + ['0']) + '\n')  #for palm
        for i in range(self.fingerNum):
            if self.fingerTypeList[i] == 'fixedBase':
                handFile.write('  '.join(['joint'] + ['weld'] + [str(i + 1)]) + '\n')
            else:   #rotaryBase
                handFile.write('  '.join(['joint'] + ['normal'] + [str(i + 1)]) + '\n')
        handFile.write('\n')
        #Controller
        handFile.write('  '.join(['servoP'] + [str(self.PID[0])] * self.controllableFingerNum) + '\n')
        handFile.write('  '.join(['servoI'] + [str(self.PID[1])] * self.controllableFingerNum) + '\n')
        handFile.write('  '.join(['servoD'] + [str(self.PID[2])] * self.controllableFingerNum) + '\n')
        handFile.write('  '.join(['dryFriction'] + [str(self.dryFriction)] * self.controllableFingerNum) + '\n')
        handFile.write('  '.join(['viscousFriction'] + [str(self.viscousFriction)] * self.controllableFingerNum) + '\n\n')        
        #Mounting the fingers
        for i in range(self.fingerNum):
            handFile.write('  '.join(['mount'] + [str(i + 1)] + ['\"' + self.fingerFileNameList[i] + '.rob' + '\"']) + ' ')
            handFile.write('  '.join([str(item) for item in (so3.identity() + [0, 0, 0])]) + '\n')
        handFile.write('\n')
        #No self collision
        handFile.write('  '.join(['noselfcollision'] + ['0 ' + str(i + 1) for i in range(self.fingerNum)]) + '\n')  #palm with finger base
        for fingerIndex in range(self.fingerNum):   #adjacent link in each finger, including its base
            handFile.write('  '.join(['noselfcollision'] + [str(self.getLinkIndex(fingerIndex, phalanxIndex)) + ' ' + str(self.getLinkIndex(fingerIndex, phalanxIndex + 1)) for phalanxIndex in range(-1, self.fingerPhalangesNumList[fingerIndex] - 1)]) + '\n')
            #palm and proximal phalanges            
            handFile.write('  '.join(['noselfcollision'] + ['0 ' + str(self.getLinkIndex(fingerIndex, 0))]) + '\n')
        handFile.write('\n')

#############################################################################################################################################################################################

class Finger():
    """
    Generates a finger robot for simulating with Klamp't.
    """
    def __init__(self, phalangesLength = [0.06, 0.05, 0.04], phalangesDiameters = [0.06, 0.05, 0.04], phalangesMass = [0.12, 0.10, 0.08]):
        """
        Creates a finger with phalanges with the given physical attributes.

        :param list[float] phalangesLength: A list of how much each phalanx
            should be scaled height-wise.
        :param list[float] phalangesDiameters: A list of how much the diameter
            of each phalanx should be scaled.
        :param list[float] phalangesMass: The masses of each phalanx.
        """        
        self.phalangesLength = phalangesLength
        self.phalangesMass = phalangesMass
        self.phalangesNum = len(phalangesLength)
        self.setGeometry(['cylinder_phalanx.trns' + str(i) + '.stl' for i in range(self.phalangesNum)])   #default setting
        self.setBaseJointOffset(0)
        self.setInitConfig([0] * self.phalangesNum)
        self.setJointPosLimit([(0, 90)] * self.phalangesNum)        
        self.setJointVelLimit([90] * self.phalangesNum)
        self.setJointAccLimit([180] * self.phalangesNum)
        self.setJointTorqueLimit([10] * self.phalangesNum)
        self.setControllerPID([50, 10 , 0])
        self.setDryFriction(0.05)
        self.setViscousFriction(2)
        self.fingerFileName = None

        for i in xrange(self.phalangesNum):
            run_blender_script(['data/hands/cylinder_phalanx.stl', '-o', 'data/hands/cylinder_phalanx.trns' + str(i) + '.stl', '-s', str(phalangesLength[i]), str(phalangesDiameters[i]), str(phalangesDiameters[i])])
        
    def setGeometry(self, phalangesGeometryFile):
        """
        Sets the geometry and physical attributes of the finger.
        Geometry file can be stl, pcd, tri, or anything else supported by
        Klamp't.
        In your geometry file, you should make sure that the:

        - Proximal joint points along the Z-axis.
        - Phalange is along the X-axis, and that the X-axis is pointing towards
            distal.
        - Y-axis is pointing towards the palm.
        - Origin is the midpoint of the joint axis.

        :param str phalangesGeometryFile: Path to the phalanx geometry file.
        """
        assert (len(phalangesGeometryFile) == self.phalangesNum), "Files dimensions doesn't match with the phalanges dimensions!"
        self.phalangesGeometryFile = phalangesGeometryFile

    def setBaseJointOffset(self, baseJointOffset):
        """
        :param float baseJointOffset: The distance between the proximal axis
            center and the base rotation certer.
        """
        self.baseJointOffset = baseJointOffset
    def setInitConfig(self, initConfig):
        """
        :param list[int] initConfig: The initial rotation of each phalanx in
            degrees.
        """
        assert (len(initConfig) == self.phalangesNum), "Initial configurations dimensions doesn't match with the phalanges dimensions!"
        self.initConfig = initConfig
     
    def setJointPosLimit(self, jointPosLimit):
        """
        :param list[list[int]] jointPosLimit: A list of the min and max angles
            each joint can rotate in degrees. The list for a finger with *N*
            joints should contain *N* lists of two integers. The first should
            be the minimum angle, and the second should be the maximum angle.
        """
        assert (len(jointPosLimit) == self.phalangesNum), "Joint positions limit dimensions doesn't match with the phalanges dimensions!"
        self.jointPosLimit = jointPosLimit

    def setJointVelLimit(self, jointVelLimit):
        """
        :param list[int] jointVelLimit: A list of the maximum angular velocity
            of each joint in deg/sec.
        """
        assert (len(jointVelLimit) == self.phalangesNum), "Joint velocities limit dimensions doesn't match with the phalanges dimensions!"
        self.jointVelLimit = jointVelLimit
        
    def setJointAccLimit(self, jointAccLimit):
        """
        :param list[int] jointAccLimit: A list of the maximum angular
            acceleration of each joint in deg/sec\ :superscript:`2`.
        """
        assert (len(jointAccLimit) == self.phalangesNum), "Joint acclerations limit dimensions doesn't match with the phalanges dimensions!"
        self.jointAccLimit = jointAccLimit
    
    def setJointTorqueLimit(self, jointTorqueLimit):
        """
        :param list[int] jointTorqueLimit: A list of the maximum torque that
            can be applied to each joint.
        """
        assert (len(jointTorqueLimit) == self.phalangesNum), "Joint acclerations limit dimensions doesn't match with the phalanges dimensions!"
        self.jointTorqueLimit = jointTorqueLimit   
    
    def setControllerPID(self, PID):
        """
        :param list[float] PID: PID values separated into a list.
        """
        assert (len(PID) == 3), "Number of input variables are not 3!" 
        self.PID = PID
    
    def setDryFriction(self, dryFriction):
        """
        :param float dryFriction: Coefficient of dry friction.
        """
        self.dryFriction = dryFriction
    
    def setViscousFriction(self, viscousFriction):
        """
        :param float viscousFriction: Coefficient of viscous friction.
        """
        self.viscousFriction = viscousFriction

    def buildFile(self, fingerFileName = 'finger_default'):
        """
        Build a Klamp't ``.rob`` file for the finger.

        :param str fingerFileName: Path to where the ``.rob`` file should be
            saved.
        """
        self.fingerFileName = fingerFileName
        fingerFile = open(HAND_FILE_PATH + fingerFileName + '.rob', 'w')
        fingerFile.write('### Finger File ###\n\n')
        #DH parameters
        fingerFile.write('  '.join(['thetaDeg'] + [str(0)] * self.phalangesNum) + '\n')        
        fingerFile.write('  '.join(['d'] + [str(0)] * self.phalangesNum) + '\n')
        fingerFile.write('  '.join(['a'] + [str(self.baseJointOffset)] + [str(item) for item in self.phalangesLength[0:-1:]]) + '\n')
        fingerFile.write('  '.join(['alphaDeg'] + [str(0)] * self.phalangesNum) + '\n\n')  
        #Rotational axes
        fingerFile.write('  '.join(['axis'] + ['0 0 1'] * self.phalangesNum) + '\n\n')   
        #Index of parent link
        fingerFile.write('  '.join(['parents'] + [str(item) for item in range(-1, self.phalangesNum - 1)]) + '\n\n')
        #Joint limits
        fingerFile.write('  '.join(['qMinDeg'] + [str(item[0]) for item in self.jointPosLimit]) + '\n')
        fingerFile.write('  '.join(['qMaxDeg'] + [str(item[1]) for item in self.jointPosLimit]) + '\n')
        fingerFile.write('  '.join(['velMaxDeg'] + [str(item) for item in self.jointVelLimit]) + '\n')
        fingerFile.write('  '.join(['accMaxDeg'] + [str(item) for item in self.jointAccLimit]) + '\n')
        fingerFile.write('  '.join(['torqueMax'] + [str(item) for item in self.jointTorqueLimit]) + '\n\n')
        #Joint initial position
        fingerFile.write('  '.join(['qDeg'] + [str(item) for item in self.initConfig]) + '\n\n')
        #Geometry files
        fingerFile.write('  '.join(['geometry'] + ['\"' + item + '\"' for item in self.phalangesGeometryFile]) + '\n')
        fingerFile.write('  '.join(['geomscale'] + ['1'] * self.phalangesNum) + '\n\n')
        #Mass
        fingerFile.write('  '.join(['mass'] + [str(item) for item in self.phalangesMass]) + '\n')
        fingerFile.write('automass\n\n')
        #Controller
        fingerFile.write('  '.join(['servoP'] + [str(self.PID[0])] * self.phalangesNum) + '\n')
        fingerFile.write('  '.join(['servoI'] + [str(self.PID[1])] * self.phalangesNum) + '\n')
        fingerFile.write('  '.join(['servoD'] + [str(self.PID[2])] * self.phalangesNum) + '\n')
        fingerFile.write('  '.join(['dryFriction'] + [str(self.dryFriction)] * self.phalangesNum) + '\n')
        fingerFile.write('  '.join(['viscousFriction'] + [str(self.viscousFriction)] * self.phalangesNum) + '\n\n')        

        fingerFile.close()

#############################################################################################################################################################################################

class GraspObject():
    """
    Generates an object for a hand to grasp in Klamp't. It allows for three
    basic object types: ``'spheres'``, ``'cylinders'`` and ``'cubes'``.
    Alternatively, you can set the object type to ``'customized'`` in order to
    provide your own geomtery.  The coordinate origin for the basic object is
    located at the center of object. Dimensions for the object are in meters.
    You can also define your own object using a stl/pcd/tri file, but please
    make sure that the coordinate origin is located at the center of the object.
    """
    def __init__(self, objectType = 'sphere', objectPos = [0, 0, 0.3], objectFriction = 0.1):
        """
        :param str objectType: What type of object to generate. It can be one
            of the predefined types, ``'sphere'``, ``'cylinder'``, or
            ``'cube'``, or ``'customized'`` in order to use external geometry.
        :param list[float] objectPos: The initial position of the object in 3D
            space.
        :param float objectFriction: The friction coefficient for the object.
        """        
        self.setObjectType(objectType)  #sphere, cylinder, cube or customized 
        self.setPosition(objectPos)      
        self.setDimension([0.1, 0.1, 0.1])
        self.setMass(0.1)    #in kg
        self.setOrient([0, 0, 0])   #z-x-z orientation representation
        self.setFriction(objectFriction)
        self.setDamping(3000)
        self.setStiffness(10000)
        self.setRestitution(0)
    
    def setObjectType(self, objectType):
        """
        :param str objectType: The type of the object. It can be one
            of the predefined types, ``'sphere'``, ``'cylinder'``, or
            ``'cube'``, or ``'customized'`` in order to use external geometry.
        """
        # If the object is not customized, set the geometry automatically
        if objectType != 'customized':
            self.objectType = objectType
            self.setGeometry()
    
    def setPosition(self, objectPos):
        """
        :param list[float] objectPos: The position of the object center of mass
            in 3D space.
        """
        assert (len(objectPos) == 3), "The input dimension should be 3!"
        self.objectPos = objectPos
        
    def setDimension(self, objectDimension):
        """
        :param list[float] objectDimension: The scaling factors for the three
            corresponding axes in the geometry file. Units are in meters.
        """
        assert (len(objectDimension) == 3), "The input dimension should be 3!"
        self.objectDimension = objectDimension
        
    def setMass(self, objectMass):
        """
        :param float objectMass: The mass of the object in kilograms.
        """
        self.objectMass = objectMass
        
    def setGeometry(self, objectGeometryFile = None):
        """
        Set the geometry for the object. If :data:`objectGeometryFile` is
        ``None``, a predefined object file corresponding to the
        :data:`objectType` is loaded. If :data:`objectGeometryFile` is not
        ``None``, the :data:`objectType` is automatically set to
        ``'customized'``.  Custom geometry files can be in stl, pcd, tri, or any
        other format the Klamp't supports. Make sure that the origin is at the
        center of the object, or else it may not be simulated properly.

        :param str objectGeometryFile: ``None`` or the path to the custom
            geometry file relative to the ``data/objects`` directory.
        """
        if objectGeometryFile == None:
            if self.objectType == 'sphere':
                self.objectGeometryFile = 'sphere.stl'
            elif self.objectType == 'cylinder':
                self.objectGeometryFile = 'cylinder.stl'
            elif self.objectType == 'cube':
                self.objectGeometryFile = 'cube.stl'
        else:
            self.objectType = 'customized'
            self.objectGeometryFile = objectGeometryFile
    
    def setOrient(self, objectOrient):
        """
        Orientation is set using the Z-Y-X representation: based on original
        coordinates in the geometry file, the object is rotated according to the
        following 3 steps:

            1. Rotate it about the Z-axis by ``Z-degrees``.
            2. Based on the new coordinates, rotate about the Y-axis by
                ``Y-degrees``.
            3. Based on the new coordinates, rotate about the X-axis by
                ``X-degrees``.

        :param list[int] objectOrient: Orientation of the object in the format
            ``[Z-degrees, Y-degrees, X-degrees]``.
        """
        self.objectOrient = objectOrient
    
    def setFriction(self, objectFriction):
        """
        :param float objectFriction: The dynamic coefficient of friction.
        """
        self.objectFriction = objectFriction
        
    def setRestitution(self, objectRestitution):
        """
        :param float objectRestitution: The coefficient of restitution.
        """
        self.objectRestitution = objectRestitution
        
    def setDamping(self, objectDamping):
        """
        :param int objectDamping: The coefficient of damping.
        """
        self.objectDamping = objectDamping
        
    def setStiffness(self, objectStiffness):
        """
        :param int objectStiffness: The stiffness of the object.
        """
        self.objectStiffness = objectStiffness
        
    def buildFile(self, objectFileName = 'object_default'):
        """
        Build a Klamp't ``.obj`` file for the grasp object.

        :param str objectFileName: Path to where the ``.obj`` file should be
            saved.
        """
        self.objectFileName = objectFileName
        objectFile = open(OBJECT_FILE_PATH + objectFileName + '.obj', 'w')
        objectFile.write('### Object File ###\n\n')
        #geometry
        objectFile.write('  '.join(['mesh'] + ['\"' + self.objectGeometryFile + '\"']) + '\n\n')
        #object pos and orient
        T1 = (so3.rotation((0, 0, 1), math.radians(self.objectOrient[0])), [0, 0, 0])
        T2 = (so3.rotation((0, 1, 0), math.radians(self.objectOrient[1])), [0, 0, 0])
        T3 = (so3.rotation((1, 0, 0), math.radians(self.objectOrient[2])), [0, 0, 0])
        T12 = se3.mul(T1, T2)
        #T3*T2*T1, T parameter is not working in the current version
        TTranslate = (so3.identity(), self.objectPos)
        TRotate = se3.mul(T12, T3)
        T = se3.mul(TTranslate, TRotate)
        objectFile.write('T  ')        
        objectFile.write(' '.join([str(round(item, 5)) for item in (T[0] + T[1])]) + '\n\n')
        #objectFile.write(' '.join(['geomtranslate'] + [str(item) for item in self.objectPos]) + '\n\n')
        #object parameters
        objectFile.write('  '.join(['geomscale'] + [str(item) for item in self.objectDimension]) + '\n')
        objectFile.write('  '.join(['mass'] + [str(self.objectMass)]) + '\n')
        objectFile.write('automass\n\n')
        objectFile.write('  '.join(['kFriction'] + [str(self.objectFriction)]) + '\n')
        objectFile.write('  '.join(['kDamping'] + [str(self.objectDamping)]) + '\n')
        objectFile.write('  '.join(['kRestitution'] + [str(self.objectRestitution)]) + '\n')
        objectFile.write('  '.join(['kStiffness'] + [str(self.objectStiffness)]) + '\n\n')
        
        objectFile.close()
        
        
        
        
                       
        
        
