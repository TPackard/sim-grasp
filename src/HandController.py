# -*- coding: utf-8 -*-
# Copyright 2015, 2016 Shiquan Wang
# 
# This file is part of SimGrasp.
# 
# SimGrasp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SimGrasp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SimGrasp.  If not, see <http://www.gnu.org/licenses/>.

# Created on Mon Dec  7 10:23:54 2015
"""
.. codeauthor:: Shiquan Wang

The Hand Controller module contains logic for controlling hands during
simulation. Since Klamp't does is not natively able to simulate tendon-based
finger movement, the Hand Controller simulates tendons by using multiple
revolute joints. The controller calculates how much torque should be applied at
each joint using a tendon-based model.
"""
import math

class HandController():
    """
    The :class:`HandController` class takes the :class:`Design.Hand` instance,
    computes the output torque based on the setting of the hand control DOF
    parameters, and stores the dependency between DOFs and corresponding
    transmission radius and stiffness.

    All joints have 0-90deg joint range, with hardstop stiffness of o.2Nm/deg by
    default. For rotary/tilted finger, don't forget to set the base joints to
    have the right joint position limits.
    """
    def __init__(self, hand, controllerType = 'underActuated'):
        """
        :param Design.Hand hand: The :class:`Design.Hand` to control.
        :param str controllerType: The type of finger actuation. Currently only
            supports ``'underActuated'``.
        """
        self.hand = hand
        self.handDofNum = len(hand.getControllableDofInfo())
        self.definedHandDofNum = 0
        self.controllerType = controllerType
        self.dofTranRadius = [0.005] * self.handDofNum
        self.dofStiffness = [0.01/90] * self.handDofNum
        self.dofPreloadTorque = [0] * self.handDofNum
        self.dofNeutralPos = [0] * self.handDofNum
        self.dofPosLimit = [(0, 90, 0.2)] * self.handDofNum
        self.definedDofSet = set() #Record DOFs that have been defined
        self.activeDofNum = 0
        self.posSensorData = []
        self.velSensorData = []

        # dofDict stores the active DOF index and its slave DOF index, as well
        # as passive DOF
        self.dofDict = dict()
        self.torqueDict = dict()
    
    def setHand(self, hand):
        """
        :param Design.Hand hand: Set the :class:`Design.Hand` to control.
        """
        self.hand = hand
        
    def setControllerType(self, controllerType):
        """
        :param str controllerType: The type of finger actuation. Currently only
            supports ``'underActuated'``.
        """
        self.controllerType = controllerType
    
    def defineActiveDof(self, activeDofIndexPair, slaveDofIndexPairList):
        """
        Set the DOFs for a finger. The active DOF is the main DOF that controls
        the slaves.
        An index pair for the input should be in the format ``(fingerIndex,
        jointIndex)``. Indices are zero-based, and an index of -1 represents the
        rotary base joint of a finger. All of the DOF data will be stored in a
        dictionary as ``(activeDofIndexPair: passiveDofIndexPairList)``. 

        :param list[int] activeDofIndexPair: The index pair of the controllable
            DOF. The index pair can be used to query DOF index with
            :func:`Design.Hand.getControllableDofIndex`.
        :param list[list[int]] slaveDofIndexList: The list of passive DOF index pairs.
        """
        (fingerIndex, jointIndex) =  activeDofIndexPair 
        assert (self.hand.getControllableDofIndex(fingerIndex, jointIndex) < self.handDofNum), 'Controllable DOF index is out of range!'         
        assert (activeDofIndexPair not in self.definedDofSet), 'The DOF has already been defined!'
        self.definedDofSet.add(activeDofIndexPair)
        for item in slaveDofIndexPairList:
            if item in self.definedDofSet:
                raise Exception("The slave DOF has already been defined!")
            self.definedDofSet.add(item)
        self.dofDict[activeDofIndexPair] = slaveDofIndexPairList
        self.activeDofNum += 1
        self.definedHandDofNum += 1 + len(slaveDofIndexPairList)
        self.torqueDict[activeDofIndexPair] = 0 #default value
    
    def setDofTranRadius(self, dofIndexPair, tranRadius):
        """
        :param list[int] dofIndexPair: The index pair of the DOF to set the
            transmission radius of.
        :param float tranRadius: The transmission radius in meters.
        """
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        self.dofTranRadius[self.hand.getControllableDofIndex(fingerIndex, jointIndex)] = tranRadius
    
    def getDofTranRadius(self, dofIndexPair):
        """
        :param list[int] dofIndexPair: The index pair of the DOF to get the
            transmission radius of.
        :return: The transmission radius of the DOF.
        :rtype: float
        """
        #TODO: assertion
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        return self.dofTranRadius[self.hand.getControllableDofIndex(fingerIndex, jointIndex)]      

    def setDofStiffness(self, dofIndexPair, stiffness):
        """
        :param list[int] dofIndexPair: The index pair of the DOF to set the
            stiffness of.
        :param int stiffness: The stiffness of the DOF in Nm/deg.
        """
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        self.dofStiffness[self.hand.getControllableDofIndex(fingerIndex, jointIndex)] = stiffness 

    def setDofPreloadTorque(self, dofIndexPair, preloadTorque):
        """ 
        Set the preload torque of a DOF. Note that positive values will have a
        direction opposing the positive torque direction.

        :param list[int] dofIndexPair: The index pair of the DOF to set the
            preload torque of.
        :param float preloadTorque: The torque of the preload spring in Nm.
        """
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        self.dofPreloadTorque[self.hand.getControllableDofIndex(fingerIndex, jointIndex)] = preloadTorque 

    def setDofNeutralPos(self, dofIndexPair, neutralPos):
        """
        :param list[int] dofIndexPair: The index pair of the DOF to set the
            neutral position of.
        :param int neutralPos: The neutral position of the spring in degrees.
        """
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        self.dofNeutralPos[self.hand.getControllableDofIndex(fingerIndex, jointIndex)] = neutralPos   
    
    def setDofPosLimit(self, dofIndexPair, lowerLimit, upperLimit, hardStopStiffness):
        """ 
        Set the hard stop limit and stiffness of given DOF.

        :param list[int] dofIndexPair: The index pair of the DOF to set the
            position limit of.
        :param int lowerLimit: The minimum position in degrees.
        :param int upperLimit: The maximum position in degrees.
        :param int hardStopStiffness: The stiffness at the positional limits in
            Nm/deg.
        """
        fingerIndex = dofIndexPair[0]
        jointIndex = dofIndexPair[1]
        self.dofPosLimit[self.hand.getControllableDofIndex(fingerIndex, jointIndex)] = (lowerLimit, upperLimit, hardStopStiffness)

    def definePassiveDof(self, passiveDofIndexPair):
        """ 
        Define a passive DOF that is excluded from all actuation chain.

        :param list[int] passiveDofIndexPair: Index pair for the passive DOF.
        """
        assert (passiveDofIndexPair not in self.definedDofSet), 'The DOF has already been defined!'
        self.definedDofSet.add(passiveDofIndexPair)
        self.dofDict[passiveDofIndexPair] = 'passive'
        self.definedHandDofNum += 1
    
    def updatePosSensorData(self, posSensorData):
        """
        Updates the known positions of the DOFs with the measured positions
        from the Klamp't simulation.

        :param list[float] posSensorData: A list of all of the positions of the
            DOFs in radians.
        """
        #raw data is in radians, change it into degrees
        self.posSensorData = [math.degrees(data) for data in posSensorData]
        
    def updateVelSensorData(self, velSensorData):
        """
        Updates the known velocities of the DOFs with the measured velocities
        from the Klamp't simulation.

        :param list[float] velSensorData: A list of the velocities of the DOFs
            in radians/second.
        """
        #raw data is in radians, change it into degrees        
        self.velSensorData = [math.degrees(data) for data in velSensorData]
    
    def setDofTorque(self, indexPair, torque):
        """
        Set the torque of a specific DOF.

        :param list[int] indexPair: The index pair of the DOF to set the
            torque of.
        :param int torque: The torque of the DOF in radians/second\ :superscript:`2`.
        """
        if not self.dofDict.has_key(indexPair):
            raise Exception("The DOF has not been defined!")
        elif self.dofDict[indexPair] == 'passive':
            raise Exception("It is a passive DOF!")
        self.torqueDict[indexPair] = torque #the torque is in Nm
        
    def getTendonPos(self, activeDofIndexPair):
        """
        Query the tendon length position along an actuation chain with respect
        to the active DOF. The positive direction is same as the direction of
        the finger joint (it increases when finger closes).

        :param list[int] activeDofIndexPair: The index pair of the active DOF to
            get the tendon position of.
        :return: The tendon position in meters.
        :rtype: float
        """
        assert (activeDofIndexPair in self.definedDofSet), 'The DOF has not been defined!'
        assert (self.dofDict[activeDofIndexPair] != 'passive'), 'The DOF is not active DOF!'
        tendonPos = 0
        tendonPos += math.radians(self.getDofPos(activeDofIndexPair)) * self.getDofTranRadius(activeDofIndexPair)
        for slaveDofIndexPair in self.dofDict[activeDofIndexPair]:
            tendonPos += math.radians(self.getDofPos(slaveDofIndexPair)) * self.getDofTranRadius(slaveDofIndexPair)
        return tendonPos
        
    def getDofPos(self, dofIndexPair):
        """
        :param list[int] dofIndexPair: The index pair of the DOF to get the
            position of.
        :return: The position of the given DOF in degrees.
        :rtype: int
        """
        #TODO: assertion
        (fingerIndex, jointIndex) = dofIndexPair
        sensorizedDofIndex = self.hand.getSensorizedDofIndex(fingerIndex, jointIndex)
        dofPos = self.posSensorData[sensorizedDofIndex]
        return dofPos
    
    def computeTorque(self):
        """
        Computes the torque for each DOF based on the current tendon position.

        :return: A list of torques for all controllable DOFs (in Nm). Use with the
            Klamp't torque command to control the hand.
        :rtype: list[float]
        """
        assert (self.definedHandDofNum == self.handDofNum), 'Not all controllable DOFs of the hand have been defined!'
        actualDofTorqueList = [0] * self.handDofNum
        if selfIcontrollerType == 'underActuated':
            for dofIndexPair in self.dofDict:
                if self.dofDict[dofIndexPair] == 'passive':
                    (fingerIndex, jointIndex) = dofIndexPair
                    dofIndex = self.hand.getControllableDofIndex(fingerIndex, jointIndex)
                    sensorizedDofIndex = self.hand.getSensorizedDofIndex(fingerIndex, jointIndex)
                    dofPos = self.posSensorData[sensorizedDofIndex]
                    actualDofTorqueList[dofIndex] = (self.dofNeutralPos[dofIndex] - dofPos) * self.dofStiffness[dofIndex] - self.dofPreloadTorque[dofIndex]
                    #handle the joint limit
                    lowerLimit, upperLimit, hardStopStiffness = self.dofPosLimit[self.hand.getControllableDofIndex(fingerIndex, jointIndex)]
                    if dofPos < lowerLimit:
                        actualDofTorqueList[dofIndex] += (lowerLimit - dofPos) * hardStopStiffness
                    elif dofPos > upperLimit:
                        actualDofTorqueList[dofIndex] += (upperLimit - dofPos) * hardStopStiffness
                    
                else:   #active DOFs and their slave DOFs
                    (fingerIndex, jointIndex) = dofIndexPair
                    dofIndex = self.hand.getControllableDofIndex(fingerIndex, jointIndex)
                    sensorizedDofIndex = self.hand.getSensorizedDofIndex(fingerIndex, jointIndex)
                    dofPos = self.posSensorData[sensorizedDofIndex]
                    actualDofTorqueList[dofIndex] = self.torqueDict[dofIndexPair] + (self.dofNeutralPos[dofIndex] - dofPos) * self.dofStiffness[dofIndex] - self.dofPreloadTorque[dofIndex]
                    #handle the joint limit
                    lowerLimit, upperLimit, hardStopStiffness = self.dofPosLimit[self.hand.getControllableDofIndex(fingerIndex, jointIndex)]
                    if dofPos < lowerLimit:
                        actualDofTorqueList[dofIndex] += (lowerLimit - dofPos) * hardStopStiffness
                    elif dofPos > upperLimit:
                        actualDofTorqueList[dofIndex] += (upperLimit - dofPos) * hardStopStiffness
                        
                    #computing torque for the slave DOFs
                    for slaveDofIndexPair in self.dofDict[dofIndexPair]:       
                        (slaveFingerIndex, slaveJointIndex) = slaveDofIndexPair
                        slaveDofIndex = self.hand.getControllableDofIndex(slaveFingerIndex, slaveJointIndex)
                        slaveSensorizedDofIndex = self.hand.getSensorizedDofIndex(slaveFingerIndex, slaveJointIndex)
                        slaveDofPos = self.posSensorData[slaveSensorizedDofIndex]
                        actualDofTorqueList[slaveDofIndex] = self.torqueDict[dofIndexPair] / self.dofTranRadius[dofIndex] * self.dofTranRadius[slaveDofIndex] + (self.dofNeutralPos[slaveDofIndex] - slaveDofPos) * self.dofStiffness[slaveDofIndex] - self.dofPreloadTorque[slaveDofIndex]         
                        #handle the joint limit
                        lowerLimit, upperLimit, hardStopStiffness = self.dofPosLimit[self.hand.getControllableDofIndex(slaveFingerIndex, slaveJointIndex)]
                        if slaveDofPos < lowerLimit:
                            actualDofTorqueList[slaveDofIndex] += (lowerLimit - slaveDofPos) * hardStopStiffness
                        elif slaveDofPos > upperLimit:
                            actualDofTorqueList[slaveDofIndex] += (upperLimit - slaveDofPos) * hardStopStiffness
        return actualDofTorqueList
        
        
        
        
        
        
