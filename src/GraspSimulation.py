# -*- coding: utf-8 -*-
# Copyright 2015, 2016 Shiquan Wang
# 
# This file is part of SimGrasp.
# 
# SimGrasp is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
# 
# SimGrasp is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with SimGrasp.  If not, see <http://www.gnu.org/licenses/>.

# Created on Wed Oct 28 12:11:06 2015

"""
.. codeauthor:: Shiquan Wang

The GraspSimulation module runs batch simulations of hands grasping objects. It
uses Klamp't to control the physics simulation.
"""

from klampt import *
from klampt.glrobotprogram import *
import numpy

#import Design
#import HandController
#import OpenGL.GLUT
WORLD_FILE_PATH = '/home/tyler/projects/stanford/sim_grasp/sim-grasp/data/'

class GraspSimulation(GLSimulationProgram):
    """
    :class:`GraspSimulation` is the core framework of the simulator. It
    interfaces with the :mod:`Design` and :mod:`HandController` modules to
    simulate hands grasping objects.

    The constructor loads the world and sets up the hand and grasp objects
    before beginning the batch simulation.

    *Note:* :func:`control_loop`, :func:`display`, :func:`motionfunc`, and
    :func:`mousefunc` will be called in every simulation step.
    """
    def __init__(self, batchSim, files = [WORLD_FILE_PATH + 'world_default.xml']):
        """
        __init__(self, batchSim, files=[PATH_TO_WORLD_FILE])

        :param GraspSimulation.BatchSim batchSim: The batch of simulations to
            simulate.
        :param list[str] files: The Klamp't world files to use for the simulation.
        """
        self.initSimDone = False    #this flag is to prevent the GUI updating when initializing the model
        self.batchSim = batchSim
        self.batchSim.importNewSim()
        #create a world from the given files
        world = WorldModel()
        for fn in files:
            res = world.readFile(fn)
            if not res:
                raise RuntimeError("Unable to load model "+fn)
        #initialize the simulation
        GLSimulationProgram.__init__(self,world,"Grasp Simulation")
        self.controller = self.sim.controller(0)
        self.simObject = self.sim.body(world.rigidObject(0))
        #self.simFingerTip = self.sim.body(self.world.robotLink(0, 8))
        
        #update the hand design related class objects to the simulator
        self.finger = self.batchSim.finger
        self.hand = self.batchSim.hand
        self.graspObject = self.batchSim.graspObject
        self.handController = self.batchSim.handController
        self.graspStateMachine = self.batchSim.graspStateMachine
        
        self.currentTime = 0   #self.dt is time step length   
        self.posSensorData = []
        self.textPos = 450
        self.initSimDone = True
        
        #check if auto start the simulation
        if self.batchSim.autoStart:
            self.simulate = True
        
    def control_loop(self):
        """
        Continually updates the simulation and resets the simulation when it is
        over. The Klamp't simulation is managed here.
        """
        self.currentTime = self.sim.getTime()   #update time    
        self.posSensorData = self.controller.getSensedConfig()  #update position sensor
        self.handController.updatePosSensorData(self.posSensorData) #update sensor data to controller
        self.graspStateMachine.runStateMachine(self.currentTime, self.handController, self.simObject) #step forward the state machine
        actualTorque = self.handController.computeTorque()  #compute the actual torque
        self.controller.setTorque(actualTorque)
        
        #check if a simulation is done to reinitialize for next simulation
        if self.graspStateMachine.simIsDone:
            self.initSimDone = False    #this flag is to prevent the GUI updating when initializing the model
            self.batchSim.simResult.append(self.graspStateMachine.result)
            self.batchSim.deleteOldSim()
            self.reinitSim()
            self.initSimDone = True

            
    def reinitSim(self):
        """
        Reinitialize the simulation with the next design and control parameters
        from the :class:`GraspSimulation.BatchSim`.
        """
        self.simulate = False
        del(self.world)
        self.batchSim.importNewSim()
        #create a world from the given files
        world = WorldModel()        
        files = [WORLD_FILE_PATH + 'world_default.xml']
        for fn in files:
            res = world.readFile(fn)
            if not res:
                raise RuntimeError("Unable to load model "+fn)
        #initialize the simulation
        GLSimulationProgram.__init__(self, world, "Grasp Simulation")
        self.controller = self.sim.controller(0)
        self.simObject = self.sim.body(world.rigidObject(0))
        #self.fingerTip = self.sim.body(self.world.robotLink(0, 8))
        
        #update the hand design related class objects to the simulator
        self.finger = self.batchSim.finger
        self.hand = self.batchSim.hand
        self.graspObject = self.batchSim.graspObject
        self.handController = self.batchSim.handController
        self.graspStateMachine = self.batchSim.graspStateMachine
        
        self.currentTime = 0   #self.dt is time step length   
        self.posSensorData = []
        
        #check if auto start the simulation
        if self.batchSim.autoStart:
            self.simulate = True

        
    def display(self):
        """
        Draws the simulation and displays information on the screen using
        OpenGL.
        """
        #the current example draws the simulated world in grey and the
        #commanded configurations in transparent green
        if self.initSimDone:
            self.sim.updateWorld()
            self.world.drawGL()
    
            #draw commanded configurations
            if self.commanded_config_color != None:
                glEnable(GL_BLEND)
                glBlendFunc(GL_SRC_ALPHA,GL_ONE_MINUS_SRC_ALPHA)
                glMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT_AND_DIFFUSE,self.commanded_config_color)
                for i in xrange(self.world.numRobots()):
                    r = self.world.robot(i)
                    mode = self.sim.controller(i).getControlType()
                    if mode == "PID":
                        q = self.sim.controller(i).getCommandedConfig()
                        r.setConfig(q)
                        r.drawGL(False)
                glDisable(GL_BLEND)
    
            #draw contacts, if enabled
            if self.drawContacts:
                glDisable(GL_LIGHTING)
                glDisable(GL_DEPTH_TEST)
                glEnable(GL_POINT_SMOOTH)
                glColor3f(1,1,0)
                glLineWidth(1.0)
                glPointSize(5.0)
                forceLen = 0.1  #scale of forces
                maxid = self.world.numIDs()
                for i in xrange(maxid):
                    for j in xrange(i+1,maxid):
                        points = self.sim.getContacts(i,j)
                        if len(points) > 0:
                            forces = self.sim.getContactForces(i,j)
                            glBegin(GL_POINTS)
                            for p in points:
                                glVertex3f(*p[0:3])
                            glEnd()
                            glBegin(GL_LINES)
                            for p,f in zip(points,forces):
                                glVertex3f(*p[0:3])
                                glVertex3f(*vectorops.madd(p[0:3],f,forceLen))
                            glEnd()                        
                glEnable(GL_DEPTH_TEST)
    
            #draw text
            glColor3f(0,0,0)
            self.textPos = 450
            glWindowPos2i(10, self.textPos)
            for c in self.graspStateMachine.textToDisplay:            
                if c == '\n':
                    self.textPos -= 35
                    glWindowPos2i(4, self.textPos)
                glutBitmapCharacter(GLUT_BITMAP_TIMES_ROMAN_24, ord(c));
        

    def mousefunc(self,button,state,x,y):
        """
        Acts upon mouse clicks. Currently only prints mouse events and passes
        mouse clicks to Klamp't.

        :param int button: The number of the mouse button whose state changed.
            ``0`` is the left button, ``1`` is the right, and ``2`` is the
            middle button.
        :param in state: The number for the new state of the button. ``0`` for
            released, ``1`` for pressed.
        :param int x: The X-coordinate of the mouse.
        :param int y: The Y-coordinate of the mouse.
        """
        #Prints out the list of objects clicked whenever
        #you right click
        print "mouse",button,state,x,y
        if button==2:
            if state==0:
                print [o.getName() for o in self.click_world(x,y)]
            return
        GLSimulationProgram.mousefunc(self,button,state,x,y)

    def motionfunc(self,x,y,dx,dy):
        """
        Acts upon mouse movements. Rotates the world with click and drag, pans
        with with ctrl click and drag, and zooms with shift click and drag. All
        events are passed to Klamp't.

        :param int x: The X-coordinate of the mouse.
        :param int y: The Y-coordinate of the mouse.
        :param int dx: The distance the mouse moved along the X-axis.
        :param int dy: The distance the mouse moved along the Y-ayis.
        """
        GLSimulationProgram.motionfunc(self,x,y,dx,dy)


class GraspStateMachine:
    """
    The state machine framework for simulation logic in a single run of
    simulation. :func:`runStateMachine` contains all of the grasp logic. This
    class also contains a grasp logic example: measuring the maximum pullout
    force of a grasp.
    """
    def __init__(self, graspTorque = 1):
        """
        :param int graspTorque: The amount of torque to apply while grasping.
        """
        self.states = ['Free_Grasping', 'Object_Grasping', 'Object_Pulling', 'Finished']
        self.graspTorque = graspTorque
        self.pullForce = 0
        self.enterState('Free_Grasping')
        self.textToDisplay = ''
        self.simIsDone = False
        self.result = None

    def runStateMachine(self, currentTime, handController, graspObject):  
        """
        Runs the state machine by one step and manages which state the
        simulation is in. You should override this with your own state machine
        code.

        :param float currentTime: The current time of the Klamp't simulation.
        :param HandController.HandController handController: The
            :class:`HandController.HandController` being used in the simulation.
        :param Design.GraspObject graspObject: The :class:`Design.GraspObject`
            being grasped in the simulation.
        """
        # Update all variables
        [objAnguVel, objTranVel] = graspObject.getVelocity()
        [objRotMat, objPos] = graspObject.getTransform()
        
        """State machine"""
        #closing the hand, before contacting the object
        if self.curState == 'Free_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            #when touching the object (the object start to move)
            if self.objIsMoving(objAnguVel, objTranVel):
                self.enterState('Object_Grasping')

        #hand is grasping the object
        elif self.curState == 'Object_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            #when the grasp enters equilibrium state, start pulling the object
            if self.objIsStill(objAnguVel, objTranVel):
                self.enterState('Object_Pulling')
            #when the object escaping away from the hand, finish the simulation
            elif self.objHasEscaped(objPos):
                self.enterState('Finished')
                
        #pulling the object along z axis, increase the pulling force when the object is still
        elif self.curState == 'Object_Pulling':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            if self.objHasEscaped(objPos):
                self.enterState('Finished')
            #when the object becomes still, increase the pulling force
            elif self.objIsStill(objAnguVel, objTranVel):
                self.pullForce += 1
                print 'Pulling force is now:', self.pullForce
            #apply pulling force to the object, along z direction, at point in local coordinates
            graspObject.applyForceAtLocalPoint([0, 0, self.pullForce], [0, 0, 0]) 
                
        #when the simulation is finished
        elif self.curState == 'Finished':
            print 'Simulation Finished!'
            print 'Grasp Torque:', self.graspTorque, 'Maximum Pulling Force:', self.pullForce
            self.simIsDone = True
            
        """save the string that you want to display on the screen here"""
        self.textToDisplay = 'Current State: ' + self.curState + '\n' + 'Pullout Force: ' + str(self.pullForce)
 
        """save the parameters that you want to log here"""
        self.result = [currentTime, self.pullForce]
        
    def enterState(self, newState):
        """
        Change to a new state and verify that the new state exists.

        :param str newState: The new state to enter.
        """
        assert (newState in self.states), "The new state is not in the states list!"
        self.curState = newState
        print 'Current State:', newState
        
    def objHasEscaped(self, objPos):
        """
        Helper function to query if the object has escaped from the hand.
        
        :param list[float] objPos: The position of the
            :class:`Design.GraspObject` in 3D space.
        """
        if objPos[2] > 0.5: #z direction
            return True
        else:
            return False
    def objIsStill(self, objAnguVel, objTranVel):
        """
        Helper function to query if the object becomes still.
        
        :param numpy.array[float] objAnguVel: The angular velocity of the
            :class:`Design.GraspObject` in vector form.
        :param numpy.array[float] objTranVel: The transverse velocity of the
            :class:`Design.GraspObject` in vector form.
        """ 
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) < 0.005 and numpy.linalg.norm(objTranVel) < 0.0005:
            return True
        else:
            return False
    def objIsMoving(self, objAnguVel, objTranVel):
        """
        Helper function to query if the object starts to move.

        :param numpy.array[float] objAnguVel: The angular velocity of the
            :class:`Design.GraspObject` in vector form.
        :param numpy.array[float] objTranVel: The transverse velocity of the
            :class:`Design.GraspObject` in vector form.
        """ 
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) > 0.01 or numpy.linalg.norm(objTranVel) > 0.001:
            return True
        else:
            return False


class BatchSim:
    """
    Framework for runing batch simulation. This class provides an interface for
    interacting with :class:`GraspSimulation` for batch simulations.

    *Note:* if :data:`autoStart` is set to False, you will need to press "S" at
    each simulation to start    
    """
    def __init__(self, autoStart = True, logFileName = 'logFile_default.txt'):
        """
        :param bool autoStart: Whether or not to start the simulation
            automatically after Klamp't is finished loading.
        :param str logFileName: The file to output the simulation data to.
        """
        
        self.simResult = []
        self.iterNum = 0
        self.autoStart = autoStart
        self.logFileName = logFileName
    def importNewSim(self):
        """
        This function will be called by the :class:`GraspSimulation` when a
        simulation is finished. Refresh your simulation parameters here
        according to the :data:`self.iterNum`. You should override this to
        generate your own simulations.
        """
        if self.iterNum >= 10:
            self.endBatchSim()
            
        # Hand Design
        finger = Design.Finger()
        finger.buildFile()
        hand = Design.Hand([0, 0, 0.1])
        
        hand.addFinger(finger, 60, [0.03, 0.03, 0.01])
        hand.addFinger(finger, 180, [-0.03, 0, 0.01])
        hand.addFinger(finger, 300, [0.03, -0.03, 0.01])
            
        hand.buildFile()
        
        # Object 
        objPos = [0, 0, 0.18]
        frictionCoef = 0.2
        graspObject = Design.GraspObject('sphere', objPos, frictionCoef)
        graspObject.setOrient([0, 90, 0])
        graspObject.setDimension([0.1, 0.1, 0.1])
        graspObject.buildFile()
        
        # Hand Controller
        handController = HandController.HandController(hand)    #for the class that is called later, it is global and visible
        handController.defineActiveDof((0, 0), [(0, 1), (0, 2)])
        handController.defineActiveDof((1, 0), [(1, 1), (1, 2)]) 
        handController.defineActiveDof((2, 0), [(2, 1), (2, 2)])
        handController.setDofTranRadius((0, 0), 0.2)
        handController.setDofTranRadius((0, 1), 0.02)
        handController.setDofTranRadius((0, 2), 0.01)
        handController.setDofTranRadius((1, 0), 0.2)
        handController.setDofTranRadius((1, 1), 0.02)
        handController.setDofTranRadius((1, 2), 0.01)
        handController.setDofTranRadius((2, 0), 0.2)
        handController.setDofTranRadius((2, 1), 0.02)
        handController.setDofTranRadius((2, 2), 0.01)    
        
        # Grasp State Machine
        graspForce = 1.2
        graspStateMachine = GraspStateMachine(graspForce)
        
        # Update for tne new parameters
        self.finger = finger
        self.hand = hand
        self.graspObject = graspObject
        self.handController = handController
        self.graspStateMachine = graspStateMachine
        
        self.iterNum += 1
        
    def deleteOldSim(self):
        """
        Delete all of the objects and the state machine from the previous
        simulation.
        """
        del(self.finger)
        del(self.hand)
        del(self.graspObject)
        del(self.handController)
        del(self.graspStateMachine)        
    
    def endBatchSim(self):
        """ 
        Save the log and end the simulation.
        """
        f = open(self.logFileName, 'w')
        for result in self.simResult:
            for item in result:
                f.write(str(item) + '\t')
            f.write('\n')
        f.close()
        exit(0)
