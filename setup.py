#!/usr/bin/env python

from distutils.core import setup
import os

currentPath = os.path.dirname(os.path.abspath(__file__))

lines = []
with open('./src/Design.py') as infile:
    for line in infile:
        line = line.replace('HAND_FILE_PATH_TO_REPLACE', currentPath + '/data/hands/')        
        line = line.replace('OBJECT_FILE_PATH_TO_REPLACE', currentPath + '/data/objects/')
        lines.append(line)
with open('./src/Design.py', 'w') as outfile:
    for line in lines:
        outfile.write(line)

lines = []
with open('./src/GraspSimulation.py') as infile:
    for line in infile:
        line = line.replace('WORLD_FILE_PATH_TO_REPLACE', currentPath + '/data/')
        lines.append(line)
with open('./src/GraspSimulation.py', 'w') as outfile:
    for line in lines:
        outfile.write(line)

setup(name='SimGrasp',
      version='0.1',
      description='SimGrasp Hand Simulator',
      author='Shiquan Wang',
      author_email='shiquan@stanford.edu',
      url='https://bitbucket.org/shiquan/sim-grasp',
      package_dir = {'simgrasp': 'src'},
      packages=['simgrasp']
     )