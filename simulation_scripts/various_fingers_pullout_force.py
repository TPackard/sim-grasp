# -*- coding: utf-8 -*-
"""
Created on Tue Jan 26 15:49:56 2016

@author: shiquan
"""

import numpy
import math

from simgrasp import Design
from simgrasp import HandController
from simgrasp import GraspSimulation



class GraspStateMachine:
    def __init__(self, hand, graspTorque = 1):
        self.states = ['Free_Grasping', 'Object_Grasping', 'Object_Pulling', 'Finished']
        self.graspTorque = graspTorque
        self.pullForce = 0
        self.enterState('Free_Grasping')
        self.textToDisplay = ''
        self.simIsDone = False
        self.result = None
        self.hand = hand
    """
    Run the state machine by one step
    - ### Write your grasp state machine here ###
    """
    def runStateMachine(self, currentTime, handController, graspObject):  
        """Updating all varaibles"""
        [objAnguVel, objTranVel] = graspObject.getVelocity()
        [objRotMat, objPos] = graspObject.getTransform()
        
        """State machine"""
        #closing the hand, before contacting the object
        if self.curState == 'Free_Grasping':
            for i in range(self.hand.fingerNum):
                handController.setDofTorque((i, 0), self.graspTorque)
            #when touching the object (the object start to move)
            if self.objIsMoving(objAnguVel, objTranVel):
                self.enterState('Object_Grasping')

        #hand is grasping the object
        elif self.curState == 'Object_Grasping':
            for i in range(self.hand.fingerNum):
                handController.setDofTorque((i, 0), self.graspTorque)
            #when the grasp enters equilibrium state, start pulling the object
            if self.objIsStill(objAnguVel, objTranVel):
                self.enterState('Object_Pulling')
            #when the object escaping away from the hand, finish the simulation
            elif self.objHasEscaped(objPos):
                self.enterState('Finished')
                
        #pulling the object along z axis, increase the pulling force when the object is still
        elif self.curState == 'Object_Pulling':
            for i in range(self.hand.fingerNum):
                handController.setDofTorque((i, 0), self.graspTorque)
            if self.objHasEscaped(objPos):
                self.enterState('Finished')
            #when the object becomes still, increase the pulling force
            elif self.objIsStill(objAnguVel, objTranVel):
                self.pullForce += 1
                print 'Pulling force is now:', self.pullForce
            #apply pulling force to the object, along z direction, at point in local coordinates
            graspObject.applyForceAtLocalPoint([0, 0, self.pullForce], [0, 0, 0]) 
                
        #when the simulation is finished
        elif self.curState == 'Finished':
            print 'Simulation Finished!'
            print 'Grasp Torque:', self.graspTorque, 'Maximum Pulling Force:', self.pullForce
            self.simIsDone = True
        
        self.textToDisplay = 'Current State: ' + self.curState + '\n' + 'Pullout Force: ' + str(self.pullForce)
        #print handController.getTendonPos((0, 0))
        #save the parameters that you want to log here
        self.result = [currentTime, self.pullForce]
        
    def enterState(self, newState):
        assert (newState in self.states), "The new state is not in the states list!"
        self.curState = newState
        print 'Current State:', newState
        
    """Helper function to query if the object has escaped away from the hand"""
    def objHasEscaped(self, objPos):
        if objPos[2] > 0.5: #z direction
            return True
        else:
            return False
    """Helper function to query if the object becomes still""" 
    def objIsStill(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) < 0.005 and numpy.linalg.norm(objTranVel) < 0.0005:
            return True
        else:
            return False
    """Helper function to query if the object starts to move""" 
    def objIsMoving(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        if numpy.linalg.norm(objAnguVel) > 0.01 or numpy.linalg.norm(objTranVel) > 0.001:
            return True
        else:
            return False
            
class BatchSim:
    """
    - This class provide an interface between class in GraspSimulation.py for batch simulation
    - autoStart: if set to False, need to press "s" at each simulation to start    
    """
    def __init__(self, autoStart = True, logFileName = 'logFile_default.txt'):
        
        self.simResult = []
        self.iterNum = 0
        self.autoStart = autoStart
        self.logFileName = logFileName
    def importNewSim(self):
        """
        - ### Write your batch simulation logic here ###
        - This function will be called in the simulation when a simulation is done
        - Please refresh your simulation parameters here according to the self.iterNum
        """

        max_finger_num = 6
        min_finger_num = 2
        finger_num = self.iterNum + min_finger_num 
        
        if self.iterNum + min_finger_num  >= max_finger_num:
            self.endBatchSim()
            
        # Hand Design
        finger = Design.Finger()
        finger.buildFile()
        hand = Design.Hand([0, 0, 0.1])
        
        spacing_angle = 360 / finger_num
        spacing_radius = 0.045
        
        for i in range(finger_num):
            orient = spacing_angle * i
            x = math.cos(math.radians(orient)) * spacing_radius
            y = math.sin(math.radians(orient)) * spacing_radius
            hand.addFinger(finger, orient, [x, y, 0])
            
        #print hand.getControllableDofInfo()
        #print hand.getControllableJointIndex(2, 2)
        hand.buildFile()
        
        # Object 
        objPos = [0, 0, 0.18]
        frictionCoef = 0.2
        graspObject = Design.GraspObject('sphere', objPos, frictionCoef)
        graspObject.setOrient([0, 90, 0])
        graspObject.setDimension([0.1, 0.1, 0.1])
        graspObject.buildFile()
        
        # Hand Controller
        handController = HandController.HandController(hand)    #for the class that is called later, it is global and visible
                
        for i in range(finger_num):
            handController.defineActiveDof((i, 0), [(i, 1), (i, 2)])
            handController.setDofTranRadius((i, 0), 0.2)
            handController.setDofTranRadius((i, 1), 0.02)
            handController.setDofTranRadius((i, 2), 0.01)
        #handController.definePassiveDof((1, -1))    #define tiltedBase as passive joint
        #handController.setDofStiffness((1, -1), 0.1)
        #handController.setDofNeutralPos((0, 0), 45)  
        
        # Grasp State Machine
        graspForce = 1
        graspStateMachine = GraspStateMachine(hand, graspForce)
        
        # Update for tne new parameters
        self.finger = finger
        self.hand = hand
        self.graspObject = graspObject
        self.handController = handController
        self.graspStateMachine = graspStateMachine
        
        self.iterNum += 1
        
    def deleteOldSim(self):
        del(self.finger)
        del(self.hand)
        del(self.graspObject)
        del(self.handController)
        del(self.graspStateMachine)        
    
    def endBatchSim(self):
        """ 
        Save the log and end the simulation
        """
        f = open(self.logFileName, 'w')
        for result in self.simResult:
            for item in result:
                f.write(str(item) + '\t')
            f.write('\n')
        f.close()
        exit(0)
        


if __name__ == "__main__":
    batchSim = BatchSim(True, 'log.txt')
    graspSim = GraspSimulation.GraspSimulation(batchSim)        
    graspSim.run()