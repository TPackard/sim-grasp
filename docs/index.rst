.. SimGrasp documentation master file, created by
   sphinx-quickstart on Tue Jul 19 11:20:49 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to SimGrasp's documentation!
====================================

Contents:

.. toctree::
   :maxdepth: 2

   manual
   modules



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

