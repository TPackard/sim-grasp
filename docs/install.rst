##################
Installation Guide
##################

(Tested on Ubuntu 12.04LTS)

1. Install Klamp't by following `the Klamp't installation instructions`_
2. Install `Numpy`_
3. Install `Blender`_
4. Download and install SimGrasp (*Note:* after installation, you cannot move or rename the package folder)
        ::

           git clone https://bitbucket.org/shiquan/sim-grasp.git
           cd sim-grasp
           sudo python setup.py install

5. To be able to recompile the documentation, you must install `Sphinx`_.

.. _the Klamp't installation instructions: http://motion.pratt.duke.edu/klampt/tutorial_install.html
.. _Numpy: http://docs.scipy.org/doc/numpy-1.10.1/user/install.html
.. _Blender: https://www.blender.org/download/
.. _Sphinx: http://www.sphinx-doc.org/en/stable/install.html
