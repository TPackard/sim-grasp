# -*- coding: utf-8 -*-
"""
Created on Thu Dec 24 17:07:34 2015

@author: shiquan
"""

"""
An example of implementing a batch grasp simulation, which includes the following steps:
- Define a GraspStateMachine class for the simulated grasp process
- Define a BatchSim class:
    according to the iteration number that is counted, create design and controller for each simulation case:
    1) create finger, 
    2) create hand, 
    3) create object, 
    4) create hand controller, 
    5) create state machine
    6) create grasp simulation which passes the hand controller
    7) update all objects to BatchSim's self parameters
- In the main function:
    1) create BatchSim object
    2) create GraspSimulation object
    3) run the simulator
"""
import numpy

from simgrasp import Design
from simgrasp import HandController
from simgrasp import GraspSimulation

class GraspStateMachine:
    def __init__(self, graspTorque = 1):
        self.states = ['Free_Grasping', 'Object_Grasping', 'Object_Pulling', 'Finished']
        self.graspTorque = graspTorque
        self.pullForce = 0
        self.enterState('Free_Grasping')
        self.textToDisplay = ''
        self.simIsDone = False
        self.result = None
    """
    Run the state machine by one step
    - ### Write your grasp state machine here ###
    """
    def runStateMachine(self, currentTime, handController, graspObject):  
        """Update all variables"""
        [objAnguVel, objTranVel] = graspObject.getVelocity()
        [objRotMat, objPos] = graspObject.getTransform()
        
        """State machine"""
        #closing the hand, before contacting the object
        if self.curState == 'Free_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            handController.setDofTorque((3, 0), self.graspTorque)
            #when touching the object (the object start to move)
            if self.objIsMoving(objAnguVel, objTranVel):
                self.enterState('Object_Grasping')

        #hand is grasping the object
        elif self.curState == 'Object_Grasping':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            handController.setDofTorque((3, 0), self.graspTorque)
            #when the grasp enters equilibrium state, start pulling the object
            if self.objIsStill(objAnguVel, objTranVel):
                self.enterState('Object_Pulling')
            #when the object escaping away from the hand, finish the simulation
            elif self.objHasEscaped(objPos):
                self.enterState('Finished')
                
        #pulling the object along z axis, increase the pulling force when the object is still
        elif self.curState == 'Object_Pulling':
            handController.setDofTorque((0, 0), self.graspTorque)
            handController.setDofTorque((1, 0), self.graspTorque)            
            handController.setDofTorque((2, 0), self.graspTorque)
            handController.setDofTorque((3, 0), self.graspTorque)
            if self.objHasEscaped(objPos):
                self.enterState('Finished')
            #when the object becomes still, increase the pulling force
            elif self.objIsStill(objAnguVel, objTranVel):
                self.pullForce += 1
                print 'Pulling force is now:', self.pullForce
            #apply pulling force to the object, along z direction, at point in local coordinates
            graspObject.applyForceAtLocalPoint([0, self.pullForce, 0], [0, 0, 0]) 
                
        #when the simulation is finished
        elif self.curState == 'Finished':
            print 'Simulation Finished!'
            print 'Grasp Torque:', self.graspTorque, 'Maximum Pulling Force:', self.pullForce
            self.simIsDone = True
        
        self.textToDisplay = 'Current State: ' + self.curState + '\n' + 'Pullout Force: ' + str(self.pullForce)
        #print handController.getTendonPos((0, 0))
        #save the parameters that you want to log here
        self.result = [currentTime, self.pullForce]
        
    def enterState(self, newState):
        assert (newState in self.states), "The new state is not in the states list!"
        self.curState = newState
        print 'Current State:', newState
        
    """Helper function to query if the object has escaped away from the hand"""
    def objHasEscaped(self, objPos):
        if objPos[1] > 0.75: #y direction
            return True
        else:
            return False
    """Helper function to query if the object becomes still""" 
    def objIsStill(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        return numpy.linalg.norm(objAnguVel) < 0.05 and numpy.linalg.norm(objTranVel) < 0.005
    """Helper function to query if the object starts to move""" 
    def objIsMoving(self, objAnguVel, objTranVel):
        #print numpy.linalg.norm(objAnguVel), numpy.linalg.norm(objTranVel)
        return numpy.linalg.norm(objAnguVel) > 0.01 or numpy.linalg.norm(objTranVel) > 0.001
            
class BatchSim:
    """
    - This class provide an interface between class in GraspSimulation.py for batch simulation
    - autoStart: if set to False, need to press "s" at each simulation to start    
    """
    def __init__(self, autoStart = True, logFileName = 'logFile_default.txt'):
        
        self.simResult = []
        self.iterNum = 1
        self.autoStart = autoStart
        self.logFileName = logFileName
    def importNewSim(self):
        """
        - ### Write your batch simulation logic here ###
        - This function will be called in the simulation when a simulation is done
        - Please refresh your simulation parameters here according to the self.iterNum
        """
        if self.iterNum >= 10:
            self.endBatchSim()
            
        # Hand Design
        finger = Design.Finger(phalangesLength=[0.04, 0.05, 0.04], phalangesDiameters=[0.06, 0.05, 0.04])
        finger.buildFile()
        hand = Design.Hand([0, 0, 0.1])
        
        dAngle = 20 - 2 * self.iterNum
        hand.addFinger(finger, dAngle, [0.03, 0.02, 0.01])
        #hand.addFinger(finger, 180, [-0.03, 0, 0.01], 'tiltedBase')
        hand.addFinger(finger, 180 - dAngle, [-0.03, 0.02, 0.01])
        hand.addFinger(finger, 180 + dAngle, [-0.03, -0.02, 0.01])
        hand.addFinger(finger, 360 - dAngle, [0.03, -0.02, 0.01])
        #print hand.getControllableDofInfo()
        #print hand.getControllableJointIndex(2, 2)
            
        hand.buildFile()
        
        # Object 
        objPos = [0, -0.2, 0.18]
        frictionCoef = 0.2
        graspObject = Design.GraspObject('customized', objPos, frictionCoef)
        graspObject.setOrient([0, 90, -90])
        graspObject.setDimension([0.08, 0.08, 0.08])
        graspObject.setGeometry('custom.trns.stl')
        graspObject.buildFile()
        
        # Hand Controller
        handController = HandController.HandController(hand)    #for the class that is called later, it is global and visible
        handController.defineActiveDof((0, 0), [(0, 1), (0, 2)])
        handController.defineActiveDof((1, 0), [(1, 1), (1, 2)]) 
        handController.defineActiveDof((2, 0), [(2, 1), (2, 2)])
        handController.defineActiveDof((3, 0), [(3, 1), (3, 2)])
        #handController.definePassiveDof((1, -1))    #define tiltedBase as passive joint
        #handController.setDofNeutralPos((1, -1), -30)   #tiltedBase neutral position
        #handController.setDofStiffness((1, -1), 0.1)
        
    #    handController.setDofNeutralPos((0, 0), 45)
    #    handController.setDofNeutralPos((1, 0), 45)
    #    handController.setDofNeutralPos((2, 0), 45)
    #    handController.setDofNeutralPos((0, 1), 30)
    #    handController.setDofNeutralPos((1, 1), 30)
    #    handController.setDofNeutralPos((2, 1), 30)
    #    handController.setDofNeutralPos((0, 2), 20) 
    #    handController.setDofNeutralPos((1, 2), 20)
    #    handController.setDofNeutralPos((2, 2), 20)
        handController.setDofTranRadius((0, 0), 0.2)
        handController.setDofTranRadius((0, 1), 0.02)
        handController.setDofTranRadius((0, 2), 0.01)
        handController.setDofTranRadius((1, 0), 0.2)
        handController.setDofTranRadius((1, 1), 0.02)
        handController.setDofTranRadius((1, 2), 0.01)
        handController.setDofTranRadius((2, 0), 0.2)
        handController.setDofTranRadius((2, 1), 0.02)
        handController.setDofTranRadius((2, 2), 0.01)    
        handController.setDofTranRadius((3, 0), 0.2)
        handController.setDofTranRadius((3, 1), 0.02)
        handController.setDofTranRadius((3, 2), 0.01)    

        # Grasp State Machine
        graspForce = 1.2
        graspStateMachine = GraspStateMachine(graspForce)
        
        # Update for tne new parameters
        self.finger = finger
        self.hand = hand
        self.graspObject = graspObject
        self.handController = handController
        self.graspStateMachine = graspStateMachine
        
        self.iterNum += 1
        
    def deleteOldSim(self):
        del(self.finger)
        del(self.hand)
        del(self.graspObject)
        del(self.handController)
        del(self.graspStateMachine)        
    
    def endBatchSim(self):
        """ 
        Save the log and end the simulation
        """
        f = open(self.logFileName, 'w')
        for result in self.simResult:
            for item in result:
                f.write(str(item) + '\t')
            f.write('\n')
        f.close()
        exit(0)
        


if __name__ == "__main__":
    batchSim = BatchSim(True, 'log.txt')
    graspSim = GraspSimulation.GraspSimulation(batchSim)        
    graspSim.run()
