**SimGrasp**
======================

Author : Shiquan Wang  
Contact : shiquan at stanford dot edu

Developed in the Biomimetics and Dexterous Manipulation Lab (BDML), Stanford University.


About
-----
SimGrasp is a convenient simulation package built upon [Klamp't] for generic hand dynamic simulation. It is capable of batch dynamic simulation for full/under-actuated hand design optimization and can efficiently handle cases of frictional contacts and loop kinematics.

Robotic hand design usually involves optimization of numerous design parameters, including finger configuration, phalanges and palm dimensions, transmission ratios, joint stiffness, preload torque etc.. Some types of hand performance evaluations require dynamic simulation of the grasping process, such as pullout force on irregular object from a grasp, dynamic grasp region etc.. However, most of the dynamic simulation packages out there are not specifically created for this application. Changing the design and relevant parameters require constructing new xml files and changing varibales in multiple files, in which case conducting a large number of simualtions for design optimization is extremely inefficient.

This project aims at providing a convenient tool for generic hand simulation, with which users can create new hand designs and tuning hand-specific parameters with only a few lines of codes and are able to focus more on batch simulation experiments and grasping process design.

This project is developed along with BDML research projects [Multi Limbed Climbing] and [Red Sea Exploratorium] at Stanford University.

Updates
-----
**V0.2 (Jan 25, 2016)**:

  * Added class BatchSim for running grasp simulation, updated related example code.
  * Added feature: display texts in the simulation (defined in GraspStateMachine).
  * Added feature: active and passive tilted base finger option in addition to the rotary base finger (defined in Design).
  * Added feature: computing tendon position along any actuation chain (defined in HandController).
  * Bugs in Design and HandController fixed. 

**V0.1 (Dec 28, 2015)** :

  * Basic framework of SimGrasp (Design, HandController, GraspSimulation and GraspStateMachine).

Installation 
-----
(Tested on Ubuntu 12.04LTS)

1. Install Klamp't by following [Klamp't Installation Instructions]
2. Install [Numpy]
3. Install [Blender]
4. Download and install SimGrasp (after install, you cannot move or rename the package folder)

```sh
git clone https://bitbucket.org/shiquan/sim-grasp.git
cd sim-grasp
sudo python setup.py install
```

5. To be able to recompile the documentation, you must install [Sphinx].

Documentation
-----
Please check [SimGrasp Manual] for more details. An example code is provided in the package.

SimGrasp consists of the following modules:

1. Design: Create finger, hand and object design files. 
2. HandController: Configure joint parameters, joint afine (under-actuated hand), control methods and compute the joint torque.
3. GraspSimulation: Interface with Klamp't, run simulations according to given grasp state machines.
4. GraspStateMachine: A framework for users to design grasping process in a single run of simulation. More templates will be updated under "/graspSM" folder.
5. BatchSim: A framework interfacing with GraspSimulation to run batch simulation.


Dependencies
------------
1. [Klamp't] and its dependencies
2. [Numpy]
3. [Blender]
4. [Sphinx]


[Blender]:https://www.blender.org/download/
[Klamp't]:http://motion.pratt.duke.edu/klampt/index.html
[Klamp't Installation Instructions]:http://motion.pratt.duke.edu/klampt/tutorial_install.html
[Numpy]:http://docs.scipy.org/doc/numpy-1.10.1/user/install.html
[Multi Limbed Climbing]:http://bdml.stanford.edu/Main/MultiLimbedClimbing
[Red Sea Exploratorium]:http://bdml.stanford.edu/Main/KaustRedSea
[SimGrasp Manual]:https://bitbucket.org/shiquan/sim-grasp/src/master/SIMGRASP_MANUAL.md
[Sphinx]:http://www.sphinx-doc.org/en/stable/install.html
